This is a [Stylus](https://github.com/openstyles/stylus) script
to make Slack's web UI suck a little bit less.

It still sucks a lot.

To use it, install Stylus, add a style for `app.slack.com`,
and paste the contents of slack.css in.

Tested only on Firefox, because why would you not use Firefox?

Pull requests welcome.
